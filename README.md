# README #

Source code for a Digital Clock made using FSM.

Spesifications
	-Using 2 buttons as user input (Set_button, Mode_button)
	-FSM frequency used is 100Hz
	-6x 7 segment LED is used as display
	-The clock will have 2 display mode which display time and date
	-This clock can be set by pressing set_button
	-while setting the clock mode_button is used to change the number by adding 1 every time mode_button is pressed
	-in Set mode set_button is used to confirm change and move to the next digit.
	-This source code will be tested on Arduino UNO