#ifndef FSM_H
#define FSM_H

#define TIMEKEEPING_MODE 0
#define SEC_INCREMENT 1
#define SETTING_MODE 2

#define TIME_1 0
#define TIME_2 2
#define TIME_3 3
#define DATE_1 4
#define DATE_2 5
#define DATE_3 6

#define HOUR_1 0
#define HOUR_2 1
#define HOUR_3 2
#define HOUR_4 3
#define MIN_1 4
#define MIN_2 5
#define MIN_3 6
#define MIN_4 7
#define SEC_1 8
#define SEC_2 9
#define SEC_3 10
#define SEC_4 11
#define YEAR_1 12
#define YEAR_2 13
#define YEAR_3 14
#define YEAR_4 15
#define MON_1 16
#define MON_2 17
#define MON_3 18
#define MON_4 19
#define DAY_1 20
#define DAY_2 21
#define DAY_3 22
#define DAY_4 23

void fsm(int input_mode, int input_set, int input_tick, int *out_mode, 
		int *out_sec, int *out_min, int *out_hour, int *out_day, int *out_mon, 
		int *out_year, int *out_reset_tick, int *state_top, int *state_low);

#endif	/* FSM_H */