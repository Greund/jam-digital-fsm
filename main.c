/*****************************************************
Chip type               : ATmega328P
Program type            : Application
AVR Core Clock frequency: 16.000000 MHz
Memory model            : Small
External RAM size       : 0
Data Stack size         : 512
*****************************************************/

#define F_CPU 16000000UL

#include <avr/io.h>
#include <avr/interrupt.h>
#include "fsm.h"
#include <util/delay.h>

//Array untuk definisi banyaknya hari dalam suatu bulan
const int mon_rule[12]={31,28,31,30,31,30,31,31,30,31,30,31};

int state_top = TIMEKEEPING_MODE; //Initial State
int state_low = TIME_1;
int hour, min, sec; //Var waktu
int year, mon, day; //Var tanggal
int mode = 0; //Pilihan mode tampilan
int count = 0;


//Fungsi untuk menerima karakter dari USART
unsigned char USART_Receive(void)
{
	//Menunggu ada data di buffer
	while ( !(UCSR0A & (1<<RXC0)) )
	;
	//Terima karakter dari buffer dan return nilainya
	return UDR0;
}

//Fungsi untuk mengirim karakter ke USART
void USART_Transmit(unsigned char data)
{
	//Menunggu buffer kosong
	while ( !( UCSR0A & (1<<UDRE0)) )
	;
	//Masukan data yang akan dikirimkan ke buffer
	UDR0 = data;
}

//Fungsi untuk mengirim string ke USART
void USART_Transmit_String(char *data)
{
	unsigned char* p_toSend;

	p_toSend = (unsigned char*)data;
	while(*p_toSend)
	{
		USART_Transmit(*p_toSend);
		p_toSend++;
	}
}

//Fungsi untuk mengubah variable menjadi terpisah
//menurut digit dan assign ke var display
void assign(int x, int y, int z, int *dis1, int *dis2, 
			int *dis3, int *dis4, int *dis5, int *dis6)
{
	*dis1=(x/10);
	*dis2=(x%10);
	*dis3=(y/10);
	*dis4=(y%10);
	*dis5=(z/10);
	*dis6=(z%10);
}

//Fungsi untuk mengubah data int menjadi nyala port pada pin 7segment
void encode (int x)
{
	char temp = 0;
		
	if (x==0)
		temp = 0b11000000;

	else if (x==1)
		temp = 0b11111001;

	else if (x== 2)
		temp = 0b10100100;

	else if (x==3)
		temp = 0b10110000;

	else if (x==4)
		temp = 0b10011001;

	else if (x==5)
		temp = 0b10010010;

	else if (x==6) 
		temp = 0b10000010;
		
	else if (x== 7) 
		temp = 0b11111000;
	
	else if (x==8)
		temp = 0b10000000;
		
	else if (x==9)
		temp = 0b10010000;

	PORTD |= 0b11110000;
	PORTD &= ((temp << 4) | 0b00001111);
	PORTB |= 0b00000111;
	PORTB &= ((temp >> 4) & (0x0F)) | 0b11111000;
}

// Timer1 output compare A interrupt service routine
ISR(TIMER1_COMPA_vect)
{
	int pin_input;
	int input_set, input_mode, input_tick;
	int out_mode, out_reset_tick;
	int out_sec, out_min, out_hour, out_day, out_mon, out_year;
	
	out_mode = mode;
	out_sec = 0;
	out_min = 0;
	out_hour = 0;
	out_day = 0;
	out_mon = 0;
	out_year = 0;
	out_reset_tick = 0;
	
	//Input
	pin_input = PIND; //Input di PD2 dan PD3
	//PD2 untuk input set
	if(~pin_input & (1 << 2))
		input_set = 0;
	else
		input_set = 1;

	//PD3 untuk input mode
	if(~pin_input & (1 << 3))
		input_mode = 0;
	else
		input_mode = 1;

	//Menghitung 1 detik
	count++;
	if(count>=100)
	{
		input_tick=1;
		count=0;
	}
	else
		input_tick = 0;

	//Fungsi FSM
	fsm(input_mode, input_set, input_tick, &out_mode, &out_sec, &out_min, 
		&out_hour, &out_day, &out_mon, &out_year, &out_reset_tick, &state_top, 
		&state_low);

	//Output
	//Perhitungan penambahan waktu
	//Detik
	sec+=out_sec;
	if(sec>=60)
	{
		sec=0;
		out_min++;
	}
	//Menit
	min+=out_min;
	if(min>=60)
	{
		min=0;
		out_hour++;
	}
	//Jam
	hour+=out_hour;
	if(hour>=24)
	{
		hour=0;
		out_day++;
	}
	//Hari
	day+=out_day;
	if(day>=mon_rule[mon-1])
	{
		//Mengatasi tahun kabisat
		if(((year%4)==0)&&(mon==2))
		{
			if(day==29)
			{
				day=0;
				out_mon++;
			}
		}
		//bulan biasa
		else
		{
			day=0;
			out_mon++;
		}
	}
	//Bulan
	mon+=out_mon;
	if(mon>=13)
	{
		mon=1;
		out_year++;
	}
	//Tahun
	year+=out_year;
	if(year >= 100)
		year = 0;
		
	//Reset perhitungan detik
	if (out_reset_tick== 1)
		count=0;
	
	//Perubahan Mode
	mode=out_mode;
}

int main(void)
{
	int en=1;//enable 7 segment
	int c_serial=0;//counter untuk pengiriman serial
	int dis1,dis2,dis3,dis4,dis5,dis6;//var yang ditampilkan
	
	dis1 = 0;
	dis2 = 0;
	dis3 = 0;
	dis4 = 0;
	dis5 = 0;
	dis6 = 0;
	
	day = 1;
	mon = 1;
	year = 0;
	hour = 0;
	min = 0;
	sec = 0;
	
	// Crystal Oscillator division factor: 1
	CLKPR = 0x80;
	CLKPR = 0x00;

	// Input/Output Ports initialization
	// Enable pull-up
	MCUCR &= ~(1 < PUD);

	// Port B initialization
	// Func7=In Func6=In Func5=In Func4=In Func3=In Func2=Out Func1=Out Func0=Out 
	// State7=T State6=T State5=T State4=T State3=T State2=T State1=T State0=T 
	DDRB = 0b00000111;//PB0 sampai PB2 sebagai output
	PORTB = 0x00;

	// Port C initialization
	// Func6=In Func5=Out Func4=Out Func3=Out Func2=Out Func1=Out Func0=Out
	// State6=T State5=T State4=T State3=T State2=T State1=T State0=T 
	DDRC = 0b00111111;//PC0 sampai PC5 sebagai output
	PORTC = 0x00;

	// Port D initialization
	// Func7=Out Func6=Out Func5=Out Func4=Out Func3=In Func2=In Func1= Func0= 
	// State7=T State6=T State5=T State4=T State3=pull-up State2=pull-up State1= State0= 
	DDRD = 0b11110000; //PD4 sampai PD7 sebagai output, PD2 dan PD3 input
	PORTD = 0x00 | (1 << 2) | (1 << 3); //Pull-up pin input

	// Timer/Counter 0 initialization
	// Clock source: System Clock
	// Clock value: Timer 0 Stopped
	// Mode: Normal top=0xFF
	// OC0A output: Disconnected
	// OC0B output: Disconnected
	TCCR0A = 0x00;
	TCCR0B = 0x00;
	TCNT0 = 0x00;
	OCR0A = 0x00;
	OCR0B = 0x00;

	// Timer/Counter 1 initialization
	// Clock source: System Clock
	// Clock value: 62.500 kHz
	// Mode: CTC top=OCR1A
	// OC1A output: Discon.
	// OC1B output: Discon.
	// Noise Canceler: Off
	// Input Capture on Falling Edge
	// Timer1 Overflow Interrupt: Off
	// Input Capture Interrupt: Off
	// Compare A Match Interrupt: On
	// Compare B Match Interrupt: Off
	TCCR1A = 0x00;
	TCCR1B = 0x0C;
	TCNT1H = 0x00;
	TCNT1L = 0x00;
	ICR1H = 0x00;
	ICR1L = 0x00;

	OCR1AH = 0x02; //0x0271 = 625 (100 Hz)
	OCR1AL = 0x71;

	OCR1BH = 0x00;
	OCR1BL = 0x00;

	// Timer/Counter 2 initialization
	// Clock source: System Clock
	// Clock value: Timer2 Stopped
	// Mode: Normal top=0xFF
	// OC2A output: Disconnected
	// OC2B output: Disconnected
	ASSR = 0x00;
	TCCR2A = 0x00;
	TCCR2B = 0x00;
	TCNT2 = 0x00;
	OCR2A = 0x00;
	OCR2B = 0x00;

	// External Interrupt(s) initialization
	// INT0: Off
	// INT1: Off
	// Interrupt on any change on pins PCINT0-7: Off
	// Interrupt on any change on pins PCINT8-14: Off
	// Interrupt on any change on pins PCINT16-23: Off
	EICRA = 0x00;
	EIMSK = 0x00;
	PCICR = 0x00;

	// Timer/Counter 0 Interrupt(s) initialization
	TIMSK0 = 0x00;

	// Timer/Counter 1 Interrupt(s) initialization
	TIMSK1 = 0x02;

	// Timer/Counter 2 Interrupt(s) initialization
	TIMSK2 = 0x00;

	// USART initialization
	// Enabling TX & RX
	UCSR0B = (1<<RXEN0)|(1<<TXEN0);
	UCSR0A = (1<<UDRE0);
	UCSR0C =  (1 << UCSZ01) | (1 << UCSZ00); //Set frame: 8data, 1 stop
	UBRR0H = 0x00;
	UBRR0L = 0x67;

	// Analog Comparator initialization
	// Analog Comparator: Off
	// Analog Comparator Input Capture by Timer/Counter 1: Off
	ACSR = 0x80;
	ADCSRB = 0x00;
	DIDR1 = 0x00;

	// ADC initialization
	// ADC disabled
	ADCSRA = 0x00;

	// SPI initialization
	// SPI disabled
	SPCR = 0x00;

	// TWI initialization
	// TWI disabled
	TWCR = 0x00;

	// Global enable interrupts
	sei();

	while (1)
	{	
		c_serial++;
		if(c_serial==100)
		{
			c_serial=0;
			//Serial debugging
			/*			
			USART_Transmit_String("Top state: ");
			switch(state_top)
			{
				case TIMEKEEPING_MODE:
					USART_Transmit_String("TIMEKEEPING_MODE");
					break;

				case SEC_INCREMENT:
					USART_Transmit_String("SEC_INCREMENT");
					break;
					
				case SETTING_MODE:
					USART_Transmit_String("SETTING_MODE");
					break;
			}
			USART_Transmit_String("\t");
			USART_Transmit_String("Low state: ");
			switch(state_top)
			{
				case TIMEKEEPING_MODE:
				{
					switch(state_low)
					{
						case TIME_1:
							USART_Transmit_String("TIME_1");
							break;

						case TIME_2:
							USART_Transmit_String("TIME_2");
							break;

						case TIME_3:
							USART_Transmit_String("TIME_3");
							break;

						case DATE_1:
							USART_Transmit_String("DATE_1");
							break;

						case DATE_2:
							USART_Transmit_String("DATE_2");
							break;

						case DATE_3:
							USART_Transmit_String("DATE_3");
							break;						
					}
					break;
				}
				case SETTING_MODE:
				{
					switch(state_low)
					{
						case HOUR_1:
							USART_Transmit_String("HOUR_1");
							break;

						case HOUR_2:
							USART_Transmit_String("HOUR_2");
							break;

						case HOUR_3:
							USART_Transmit_String("HOUR_3");
							break;

						case HOUR_4:
							USART_Transmit_String("HOUR_4");
							break;

						case MIN_1:
							USART_Transmit_String("MIN_1");
							break;

						case MIN_2:
							USART_Transmit_String("MIN_2");
							break;
				
						case MIN_3:
							USART_Transmit_String("MIN_3");
							break;
				
						case MIN_4:
							USART_Transmit_String("MIN_4");
							break;
				
						case SEC_1:
							USART_Transmit_String("SEC_1");
							break;
				
						case SEC_2:
							USART_Transmit_String("SEC_2");
							break;
				
						case SEC_3:
							USART_Transmit_String("SEC_3");
							break;
				
						case SEC_4:
							USART_Transmit_String("SEC_4");
							break;
				
						case YEAR_1:
							USART_Transmit_String("YEAR_1");
							break;
				
						case YEAR_2:
							USART_Transmit_String("YEAR_2");
							break;
				
						case YEAR_3:
							USART_Transmit_String("YEAR_3");
							break;
				
						case YEAR_4:
							USART_Transmit_String("YEAR_4");
							break;
				
						case MON_1:
							USART_Transmit_String("MON_1");
							break;
				
						case MON_2:
							USART_Transmit_String("MON_2");
							break;
				
						case MON_3:
							USART_Transmit_String("MON_3");
							break;
				
						case MON_4:
							USART_Transmit_String("MON_4");
							break;
				
						case DAY_1:
							USART_Transmit_String("DAY_1");
							break;
				
						case DAY_2:
							USART_Transmit_String("DAY_2");
							break;
				
						case DAY_3:
							USART_Transmit_String("DAY_3");
							break;
				
						case DAY_4:
							USART_Transmit_String("DAY_4");
							break;
					}
					break;
				}
			}
			USART_Transmit('\n');
			*/
		}
		
		//Assign ke Display
		if (mode==0 || mode==2 || mode==3 || mode==4)
			assign(hour, min, sec, &dis1, &dis2, &dis3, &dis4, &dis5, &dis6);
		
		else if(mode==1 || mode==5 || mode==6 || mode==7)
			assign(year, mon, day, &dis1, &dis2, &dis3, &dis4, &dis5, &dis6);
		
		
		//Scanning 7 Segment
		PORTC = (1<<PC0)|(1<<PC1)|(1<<PC2)|(1<<PC3)|(1<<PC4)|(1<<PC5);
		if (en == 1)
		{
			if((((mode == 2) || (mode == 5)) && (c_serial < 50)) || 
			(mode == 0) || (mode == 1) || (mode == 3) || (mode == 4) 
			|| (mode == 6)  || (mode == 7))
			{
				encode(dis1); //Assign nilai baru ke port segment
				PORTC = (0<<PC0)|(1<<PC1)|(1<<PC2)|(1<<PC3)|(1<<PC4)|(1<<PC5);
			}
		}
		else if (en == 2)
		{
			if((((mode == 2) || (mode == 5)) && (c_serial < 50)) || 
			(mode == 0) || (mode == 1) || (mode == 3) || (mode == 4) 
			|| (mode == 6) || (mode == 7))
			{
				encode(dis2); //Assign nilai baru ke port segment
				PORTC = (1<<PC0)|(0<<PC1)|(1<<PC2)|(1<<PC3)|(1<<PC4)|(1<<PC5);
			}
		}
		else if (en == 3)
		{
			if((((mode == 3) || (mode == 6)) && (c_serial < 50)) || 
			(mode == 0) || (mode == 1) || (mode == 2) || (mode == 4) 
			|| (mode == 5) || (mode == 7))
			{
				encode(dis3); //Assign nilai baru ke port segment
				PORTC = (1<<PC0)|(1<<PC1)|(0<<PC2)|(1<<PC3)|(1<<PC4)|(1<<PC5);
			}
		}
		else if (en == 4)
		{
			if((((mode == 3) || (mode == 6)) && (c_serial < 50)) || 
			(mode == 0) || (mode == 1) || (mode == 2) || (mode == 4) 
			|| (mode == 5) || (mode == 7))
			{
				encode(dis4); //Assign nilai baru ke port segment
				PORTC = (1<<PC0)|(1<<PC1)|(1<<PC2)|(0<<PC3)|(1<<PC4)|(1<<PC5);
			}
		}
		else if (en == 5)
		{
			if((((mode == 4) || (mode == 7)) && (c_serial < 50)) || 
			(mode == 0) || (mode == 1) || (mode == 2) || (mode == 3)
			|| (mode == 5) || (mode == 6))
			{
				encode(dis5); //Assign nilai baru ke port segment
				PORTC = (1<<PC0)|(1<<PC1)|(1<<PC2)|(1<<PC3)|(0<<PC4)|(1<<PC5);
			}
		}
		else if (en == 6)
		{
			if((((mode == 4) || (mode == 7)) && (c_serial < 50)) ||
			(mode == 0) || (mode == 1) || (mode == 2) || (mode == 3) 
			|| (mode == 5) || (mode == 6))
			{
				encode(dis6); //Assign nilai baru ke port segment
				PORTC = (1<<PC0)|(1<<PC1)|(1<<PC2)|(1<<PC3)|(1<<PC4)|(0<<PC5);
			}
			en=0;
		}
		en += 1;
		_delay_ms(3);
	}
}