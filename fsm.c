#include "fsm.h"

//Implementasi state machine dengan software

//Variabel input
//input_mode == 0 -> button mode ditekan
//input_set == 0 -> button set ditekan
//input_tick == 1 -> saatnya detik bertambah

//Variabel output
//out_mode == 0 -> mode jam, 1 -> mode tanggal
//out_sec, out_min, out_hour == 1 -> tambahkan detik, menit, jam
//out_day, out_mon, out_year == 1 -> tambahkan tanggal, bulan, tahun
//out_reset_tick == 1 -> indikator untuk mereset timer di program utama untuk memulai hitungan detik dari awal
//state_top -> top-level-state untuk state machine yang digunakan
//state_low -> low-level-state untuk state machine yang digunakan

void fsm(int input_mode, int input_set, int input_tick, int *out_mode, 
		int *out_sec, int *out_min, int *out_hour, int *out_day, int *out_mon, 
		int *out_year, int *out_reset_tick, int *state_top, int *state_low)
{
	switch(*state_top)
	{
		case TIMEKEEPING_MODE:
		{
			if(input_tick == 1)
				*state_top = SEC_INCREMENT;
			
			else
			{
				*state_top = TIMEKEEPING_MODE;
				
				switch(*state_low)
				{
					case TIME_1:
					{
						if(input_set == 0)
							*state_low = TIME_3;
						
						else if(input_mode == 1)
							*state_low = TIME_1;
						
						else if(input_mode == 0)
							*state_low = TIME_2;
						
						break;
					}
					case TIME_2:
					{
						if(input_mode == 0)
							*state_low = TIME_2;
						
						else if(input_mode == 1)
							*state_low = DATE_1;
						
						break;
					}
					case TIME_3:
					{
						if(input_set == 0)
							*state_low = TIME_3;
						
						else if(input_set == 1)
						{
							*state_top = SETTING_MODE;
							*state_low = HOUR_1;
						}
						break;
					}
					case DATE_1:
					{
						if(input_set == 0)
							*state_low = DATE_3;
						
						else if(input_mode == 1)
							*state_low = DATE_1;
						
						else if(input_mode == 0)
							*state_low = DATE_2;
						
						break;
					}
					case DATE_2:
					{
						if(input_mode == 0)
							*state_low = DATE_2;
						
						else if(input_mode == 1)
							*state_low = TIME_1;
						
						break;
					}
					case DATE_3:
					{
						if(input_set == 0)
							*state_low = DATE_3;
						
						else if(input_set == 1)
						{
							*state_top = SETTING_MODE;
							*state_low = YEAR_1;
						}
						break;
					}
					default:
					{
						
					}
				}
			}
			break;
		}

		case SEC_INCREMENT:
		{
			*state_top = TIMEKEEPING_MODE;
			break;
		}
		
		case SETTING_MODE:
		{
			*state_top = SETTING_MODE;
			
			switch(*state_low)
			{
				case HOUR_1:
				{			
					if(input_mode == 0)
						*state_low = HOUR_2;
					
					else if(input_set == 0)
						*state_low = HOUR_3;
						
					else if(input_set == 1)
						*state_low = HOUR_1;
					
					break;
				}
				case HOUR_2:
				{
					if(input_mode == 0)
						*state_low = HOUR_2;
					
					else if(input_mode == 1)
						*state_low = HOUR_4;
					
					break;
				}
				case HOUR_3:
				{
					if(input_set == 0)
						*state_low = HOUR_3;
					
					else if(input_set == 1)
						*state_low = MIN_1;
					
					break;
				}
				case HOUR_4:
				{
					*state_low = HOUR_1;
					break;
				}
				case MIN_1:
				{	
					if(input_mode == 0)
						*state_low = MIN_2;
					
					else if(input_set == 0)
						*state_low = MIN_3;
					
					else if(input_set == 1)
						*state_low = MIN_1;
					
					break;
				}
				case MIN_2:
				{
					if(input_mode == 0)
						*state_low = MIN_2;
					
					else if(input_mode == 1)
						*state_low = MIN_4;
					
					break;
				}
				case MIN_3:
				{
					if(input_set == 0)
						*state_low = MIN_3;
					
					else if(input_set == 1)
						*state_low = SEC_1;
					
					break;
				}
				case MIN_4:
				{
					*state_low = MIN_1;
					break;
				}
				case SEC_1:
				{	
					if(input_mode == 0)
						*state_low = SEC_2;
					
					else if(input_set == 0)
						*state_low = SEC_3;
					
					else if(input_set == 1)
						*state_low = SEC_1;
					
					break;
				}
				case SEC_2:
				{
					if(input_mode == 0)
						*state_low = SEC_2;
					
					else if(input_mode == 1)
						*state_low = SEC_4;
					
					break;
				}
				case SEC_3:
				{
					if(input_set == 0)
						*state_low = SEC_3;
					
					else if(input_set == 1)
					{
						*state_top = TIMEKEEPING_MODE;
						*state_low = TIME_1;
					}
					break;
				}
				case SEC_4:
				{
					*state_low = SEC_1;
					break;
				}
				case YEAR_1:
				{	
					if(input_mode == 0)
						*state_low = YEAR_2;
					
					else if(input_set == 0)
						*state_low = YEAR_3;
					
					else if(input_set == 1)
						*state_low = YEAR_1;
					
					break;
				}
				case YEAR_2:
				{
					if(input_mode == 0)
						*state_low = YEAR_2;
					
					else if(input_mode == 1)
						*state_low = YEAR_4;
					
					break;
				}
				case YEAR_3:
				{
					if(input_set == 0)
						*state_low = YEAR_3;
					
					else if(input_set == 1)
						*state_low = MON_1;
					
					break;
				}
				case YEAR_4:
				{
					*state_low = YEAR_1;
					break;
				}
				case MON_1:
				{	
					if(input_mode == 0)
						*state_low = MON_2;
					
					else if(input_set == 0)
						*state_low = MON_3;
					
					else if(input_set == 1)
						*state_low = MON_1;
					
					break;
				}
				case MON_2:
				{
					if(input_mode == 0)
						*state_low = MON_2;
					
					else if(input_mode == 1)
						*state_low = MON_4;
					
					break;
				}
				case MON_3:
				{
					if(input_set == 0)
						*state_low = MON_3;
					
					else if(input_set == 1)
						*state_low = DAY_1;
					
					break;
				}
				case MON_4:
				{
					*state_low = MON_1;
					break;
				}
				case DAY_1:
				{	
					if(input_mode == 0)
						*state_low = DAY_2;
					
					else if(input_set == 0)
						*state_low = DAY_3;
					
					else if(input_set == 1)
						*state_low = DAY_1;
					
					break;
				}
				case DAY_2:
				{
					if(input_mode == 0)
						*state_low = DAY_2;
					
					else if(input_mode == 1)
						*state_low = DAY_4;
					
					break;
				}
				case DAY_3:
				{
					if(input_set == 0)
						*state_low = DAY_3;
					
					else if(input_set == 1)
					{
						*state_top = TIMEKEEPING_MODE;
						*state_low = DATE_1;
					}
					break;
				}
				case DAY_4:
				{
					*state_low = DAY_1;
					break;
				}
				default:
				{
				
				}
			}
			break;
		}
		
		default:
		{
			
		}
	}

	//Perhitungan output
	switch(*state_top)
	{
		case TIMEKEEPING_MODE:
		{
			switch(*state_low)
			{
				case TIME_1:
				case TIME_2:
				case TIME_3:
				{
					*out_mode = 0;
					break;
				}
				case DATE_1:
				case DATE_2:
				case DATE_3:
				{
					*out_mode = 1;
					break;
				}
				default:
				{
					
				}
			}
			break;
		}

		case SEC_INCREMENT:
		{
			*out_sec = 1;
			break;
		}
		
		case SETTING_MODE:
		{
			switch(*state_low)
			{
				case HOUR_1:
				case HOUR_2:
				case HOUR_3:
				{
					*out_mode = 2;
					break;
				}
				case HOUR_4:
				{
					*out_mode = 2;
					*out_hour = 1;
					break;
				}
				case MIN_1:
				case MIN_2:
				case MIN_3:
				{
					*out_mode = 3;
					break;
				}
				case MIN_4:
				{
					*out_mode = 3;
					*out_min = 1;
					break;
				}
				case SEC_1:
				case SEC_2:
				{
					*out_mode = 4;
					break;
				}
				case SEC_3:
				{
					*out_mode = 4;
					*out_reset_tick = 1;
					break;
				}
				case SEC_4:
				{
					*out_mode = 4;
					*out_sec = 1;
					break;
				}
				case YEAR_1:
				case YEAR_2:
				case YEAR_3:
				{
					*out_mode = 5;
					break;
				}
				case YEAR_4:
				{
					*out_mode = 5;
					*out_year = 1;
					break;
				}
				case MON_1:
				case MON_2:
				case MON_3:
				{
					*out_mode = 6;
					break;
				}
				case MON_4:
				{
					*out_mode = 6;
					*out_mon = 1;
					break;
				}
				case DAY_1:
				case DAY_2:
				{
					*out_mode = 7;
					break;
				}
				case DAY_3:
				{
					*out_mode = 7;
					*out_reset_tick = 1;
					break;
				}
				case DAY_4:
				{
					*out_mode = 7;
					*out_day = 1;
					break;
				}
				default:
				{
				
				}
			}
			break;
		}
		
		default:
		{
			
		}
	}
}